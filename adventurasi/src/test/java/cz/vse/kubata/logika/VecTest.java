
package cz.vse.kubata.logika;


import cz.vse.kubata.logika.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


import static org.junit.Assert.*;



/*******************************************************************************
 * Testovací třída VecTest slouží ke komplexnímu otestování
 * třídy Vec.
 *
 * @author  Jan Kubata
 * @version 1.00.0000 — 2020-06-18
 */
public class VecTest
{

    /***************************************************************************
     * Inicializace předcházející spuštění každého testu a připravující tzv.
     * přípravek (fixture), což je sada objektů, s nimiž budou testy pracovat.
     */
    @Before
    public void setUp()
    {
    }


    /***************************************************************************
     * Úklid po testu - tato metoda se spustí po vykonání každého testu.
     */
    @After
    public void tearDown()
    {
    }



//\TT== TESTS PROPER ===========================================================

    /**
     * Testy, které testují funkčnost a metody třídy Věc.
     */
    @Test
    public void testVeci()
    {
        Prostor prostor1 = new Prostor(null,null);
        Vec vec1 = new Vec("koš", true);
        Vec vec2 = new Vec("auto", false);
        prostor1.vlozVec(vec1);
        prostor1.vlozVec(vec2);
        assertEquals(true, prostor1.jeVecVProstoru("koš"));
        assertEquals(true, prostor1.jeVecVProstoru("auto"));
        assertEquals(false, prostor1.jeVecVProstoru("kolo"));
        assertNotNull(prostor1.seberVec("koš"));
        assertEquals(false, prostor1.jeVecVProstoru("koš"));
        assertNull(prostor1.seberVec("auto"));
        assertEquals(true, prostor1.jeVecVProstoru("auto"));
        assertNull(prostor1.seberVec("kolo"));
    }

    /**
     * Testy, které testují funkčnost a metody třídy Věc.
     */
    @Test
    public void testVecVeVeci()
    {
        Vec vec1 = new Vec("trezor",false);
        Vec vec2 = new Vec("peníze",true);
        Vec vec3 = new Vec("jízdenka",true);
        assertEquals(false, vec1.obsahujeVecTutoVec("peníze"));
        vec1.vlozVecDoVeci(vec2);
        assertEquals(false, vec1.obsahujeVecTutoVec("peníze"));
        vec1.setLzeOdemknout(true);
        vec1.vlozVecDoVeci(vec3);
        assertEquals(false, vec1.obsahujeVecTutoVec("jízdenka"));
        assertEquals(false, vec1.isOdemknuta());
        vec1.setOdemknuta(true);
        assertEquals(true, vec1.obsahujeVecTutoVec("jízdenka"));
        assertEquals(false, vec1.obsahujeVecTutoVec("peníze"));
        vec1.vlozVecDoVeci(vec2);
        assertEquals(true, vec1.obsahujeVecTutoVec("peníze"));
        vec1.setOdemknuta(false);
        assertEquals(false, vec1.obsahujeVecTutoVec("peníze"));
        assertNotNull(vec1.vratObsahVeci());
    }
}
