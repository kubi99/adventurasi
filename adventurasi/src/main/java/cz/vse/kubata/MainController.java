package cz.vse.kubata;

import cz.vse.kubata.logika.IHra;
import cz.vse.kubata.logika.Prostor;
import cz.vse.kubata.logika.Vec;
import cz.vse.kubata.logika.Batoh;
import cz.vse.kubata.logika.HerniPlan;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;

import java.util.Collection;

public class MainController {


    public TextArea textOutput;
    public TextField textInput;
    private IHra hra;

    public Label locationName;
    public Label locationDescription;

    public VBox exits;
    public VBox items;
    public VBox backpack;

    public void init(IHra hra) {
        this.hra = hra;
        update();
    }

    private void update() {

        String location = getAktualniProstor().getNazev();
        locationName.setText(location);

        String description = getAktualniProstor().getPopis();
        locationDescription.setText(description);

        updateExits();
        updateItems();
        updateBacpack();
    }

    private Prostor getAktualniProstor() {
    return hra.getHerniPlan().getAktualniProstor();}

    private void updateItems() {
        Collection<Vec> itemList = getAktualniProstor().getVeci().values();
        items.getChildren().clear();

        for (Vec item : itemList) {
            String itemName = item.getNazev();
            Label itemLabel = new Label(itemName);
            if (item.isPrenositelna()) {
                itemLabel.setCursor(Cursor.HAND);
                itemLabel.setOnMouseClicked(event -> {
                    String result = hra.zpracujPrikaz("seber " + itemName);

                    textOutput.appendText(result);
                    update();
                });
            }
            items.getChildren().add(itemLabel);
        }

    }

    private Batoh getSeznamVeci(){
        return hra.getHerniPlan().getBatoh();
    }


    private void updateBacpack(){
        Collection<Vec> backpackList = getSeznamVeci().getVeciZBatohu().values();
        backpack.getChildren().clear();

        for (Vec backpackItem : backpackList){
            String backpackItemName = backpackItem.getNazev();
            Label backpackItemLabel = new Label (backpackItemName);

            backpackItemLabel.setCursor(Cursor.HAND);
            backpackItemLabel.setOnMouseClicked(event -> {
                String result = hra.zpracujPrikaz("polož "+backpackItemName);
                hra.getHerniPlan().getBatoh().odeberVec(backpackItemName);
                textOutput.appendText(result + "\n\n" );

                update();
            });

           backpack.getChildren().add(backpackItemLabel);

        }

    }


    private void updateExits() {
        Collection<Prostor> exitList = getAktualniProstor().getVychody();
        exits.getChildren().clear();

        for (Prostor prostor : exitList) {
            String exitName = prostor.getNazev();
            Label exitLabel = new Label(exitName);
            exitLabel.setCursor(Cursor.HAND);
            exitLabel.setOnMouseClicked(event -> {
                String result = hra.zpracujPrikaz("jdi " + exitName);
                textOutput.appendText(result + "\n\n");
                update();
            });

            exits.getChildren().add(exitLabel);
        }


    }
}
